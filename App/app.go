package main

import (
	"context"
	"flag"
	"fmt"
	"net"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/gogo/status"
	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_zap "github.com/grpc-ecosystem/go-grpc-middleware/logging/zap"
	grpc_validator "github.com/grpc-ecosystem/go-grpc-middleware/validator"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	uuid "github.com/satori/go.uuid"
	service "gitlab.com/service31/Business/Service"
	common "gitlab.com/service31/Common"
	logger "gitlab.com/service31/Common/Logger"
	entity "gitlab.com/service31/Data/Entity/Business"
	module "gitlab.com/service31/Data/Module/Business"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

var (
	echoEndpoint  = flag.String("endpoint", "localhost:8000", "endpoint")
	serverChannel chan int
)

type server struct {
	module.UnimplementedBusinessServiceServer
}

func (s *server) Create(ctx context.Context, in *module.CreateBusinessRequest) (*module.BusinessDetailedDTO, error) {
	entityID, err := uuid.FromString(in.GetEntityID())
	if err != nil || entityID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad EntityID")
	}
	adminUserID, err := uuid.FromString(in.GetAdminUserID())
	if err != nil || adminUserID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad AdminUserID")
	}
	business, err := service.CreateBusiness(entityID, adminUserID, in)
	if err != nil {
		return nil, err
	}
	dto, err := business.ToDTO()
	if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return dto, nil
}
func (s *server) GetDetailedBusiness(ctx context.Context, in *module.GetDetailedBusinessRequest) (*module.BusinessDetailedDTO, error) {
	id, err := uuid.FromString(in.GetID())
	if err != nil || id == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad ID")
	}
	business, err := service.GetDetailedBusiness(id)
	if err != nil {
		return nil, err
	}
	dto, err := business.ToDTO()
	if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return dto, nil
}
func (s *server) GetCurrentBusiesses(ctx context.Context, in *module.Empty) (*module.RepeatedBusinessSimpleDTO, error) {
	claim := ctx.Value(common.ClaimMetaKey).(*jwt.StandardClaims)
	if claim.Subject == "Service" {
		return nil, status.Error(codes.PermissionDenied, "Service account doesn't allowed to call this")
	}
	adminUserID, err := uuid.FromString(claim.Id)
	if err != nil || adminUserID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad AdminUserID")
	}
	businesses, err := service.GetCurrentBusinesses(adminUserID)
	if err != nil {
		return nil, err
	}
	var dtos []*module.BusinessSimpleDTO
	for _, b := range businesses {
		dto, err := b.ToSimpleDTO()
		if logger.CheckError(err) {
			return nil, status.Error(codes.Internal, err.Error())
		}
		dtos = append(dtos, dto)
	}

	return &module.RepeatedBusinessSimpleDTO{Results: dtos}, nil
}
func (s *server) GetBusinesses(ctx context.Context, in *module.GetBusinessesRequest) (*module.RepeatedBusinessSimpleDTO, error) {
	var ids []uuid.UUID
	for _, id := range in.GetID() {
		i, err := uuid.FromString(id)
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Invalid ID: %s", id))
		}
		ids = append(ids, i)
	}
	businesses, err := service.GetBusinesses(ids)
	if err != nil {
		return nil, err
	}
	var dtos []*module.BusinessSimpleDTO
	for _, b := range businesses {
		dto, err := b.ToSimpleDTO()
		if logger.CheckError(err) {
			return nil, status.Error(codes.Internal, fmt.Sprintf("BusinessID: %s has error. ErrorMessage: %s", b.ID.String(), err.Error()))
		}
		dtos = append(dtos, dto)
	}

	return &module.RepeatedBusinessSimpleDTO{Results: dtos}, nil
}
func (s *server) GetBusinessesByEntity(ctx context.Context, in *module.GetBusinessesByEntityRequest) (*module.RepeatedBusinessSimpleDTO, error) {
	entityID, err := uuid.FromString(in.GetEntityID())
	if err != nil || entityID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad EntityID")
	}
	businesses, err := service.GetBusinessesByEntityID(entityID)
	if err != nil {
		return nil, err
	}
	var dtos []*module.BusinessSimpleDTO
	for _, b := range businesses {
		dto, err := b.ToSimpleDTO()
		if logger.CheckError(err) {
			return nil, status.Error(codes.Internal, err.Error())
		}
		dtos = append(dtos, dto)
	}

	return &module.RepeatedBusinessSimpleDTO{Results: dtos}, nil
}
func (s *server) Update(ctx context.Context, in *module.UpdateBusinessRequest) (*module.BusinessDetailedDTO, error) {
	id, err := uuid.FromString(in.GetID())
	if err != nil || id == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad ID")
	}
	business, err := service.UpdateBusiness(id, in)
	if err != nil {
		return nil, err
	}
	dto, err := business.ToDTO()
	if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return dto, nil
}
func (s *server) Delete(ctx context.Context, in *module.DeleteBusinessRequest) (*module.BoolResponse, error) {
	id, err := uuid.FromString(in.GetID())
	if err != nil || id == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad ID")
	}
	result, err := service.DeleteBusiness(id)
	if err != nil {
		return nil, err
	}
	return &module.BoolResponse{IsSuccess: result}, nil
}

func runGRPCServer() {
	lis, err := net.Listen("tcp", ":8000")
	logger.CheckFatal(err)

	s := grpc.NewServer(
		grpc.StreamInterceptor(
			grpc_middleware.ChainStreamServer(
				grpc_validator.StreamServerInterceptor(),
				service.StreamServerAuthInterceptor(),
				grpc_zap.StreamServerInterceptor(logger.GetLogger()),
			)),
		grpc.UnaryInterceptor(
			grpc_middleware.ChainUnaryServer(
				grpc_validator.UnaryServerInterceptor(),
				service.UnaryServerAuthInterceptor(),
				grpc_zap.UnaryServerInterceptor(logger.GetLogger()),
			)),
	)
	module.RegisterBusinessServiceServer(s, &server{})
	if err := s.Serve(lis); err != nil {
		logger.Error(fmt.Sprintf("failed to serve: %v", err))
	}

	serverChannel <- 1
}

func serveSwagger(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "./Swagger/business.swagger.json")
}

func runRestServer() {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	mux := http.NewServeMux()
	gwmux := runtime.NewServeMux()
	opts := []grpc.DialOption{grpc.WithInsecure()}
	err := module.RegisterBusinessServiceHandlerFromEndpoint(ctx, gwmux, *echoEndpoint, opts)
	if logger.CheckError(err) {
		serverChannel <- 999
		return
	}
	mux.Handle("/", gwmux)
	mux.HandleFunc("/swagger.json", serveSwagger)
	fs := http.FileServer(http.Dir("Swagger/swagger-ui"))
	mux.Handle("/swagger-ui/", http.StripPrefix("/swagger-ui", fs))

	logger.CheckError(http.ListenAndServe(":80", mux))
	serverChannel <- 2
}

func main() {
	common.Bootstrap()
	common.DB.AutoMigrate(&entity.Business{}, &entity.OpenSchedule{}, &entity.CloseSchedule{})
	serverChannel = make(chan int)
	go runGRPCServer()
	go runRestServer()
	for {
		serverType := <-serverChannel
		if serverType == 1 {
			//GRPC
			logger.Info("Restarting GRPC endpoint")
			go runGRPCServer()
		} else if serverType == 2 {
			//Rest
			logger.Info("Restarting Rest endpoint")
			go runRestServer()
		} else {
			logger.Fatal("Failed to start some endPoint")
		}
	}
}
