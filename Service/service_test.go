package service

import (
	"testing"

	uuid "github.com/satori/go.uuid"
	common "gitlab.com/service31/Common"
	entity "gitlab.com/service31/Data/Entity/Business"
)

func TestMain(m *testing.M) {
	SetupTesting()
	common.DB.AutoMigrate(&entity.Business{}, &entity.OpenSchedule{}, &entity.CloseSchedule{})
	defer DoneTesting()
	m.Run()
}

func TestCreateBusiness(t *testing.T) {
	entityID := uuid.NewV4()
	adminUserID := uuid.NewV4()
	createIn := GetNewBusinessesTestData(entityID, adminUserID)

	business, err := CreateBusiness(entityID, adminUserID, createIn[0])

	if err != nil {
		t.Error(err)
	} else if business.Description != createIn[0].GetDescription() {
		t.Error("Description doesn't match")
	}
}

func TestGetDetailedBusiness(t *testing.T) {
	entityID := uuid.NewV4()
	adminUserID := uuid.NewV4()
	createIn := GetNewBusinessesTestData(entityID, adminUserID)
	business, err := CreateBusiness(entityID, adminUserID, createIn[0])
	if err != nil {
		t.Error(err)
	} else if business.Description != createIn[0].GetDescription() {
		t.Error("Description doesn't match")
	}
	SetupBusiness(business)

	business, err = GetDetailedBusiness(business.ID)

	if err != nil {
		t.Error(err)
	}
}

func TestGetDetailedBusinessWithoutBusiness(t *testing.T) {
	_, err := GetDetailedBusiness(uuid.NewV4())

	if err == nil {
		t.Error("No error returned")
	}
}

func TestGetCurrentBusinesses(t *testing.T) {
	entityID := uuid.NewV4()
	adminUserID := uuid.NewV4()
	createIn := GetNewBusinessesTestData(entityID, adminUserID)
	for _, r := range createIn {
		business, err := CreateBusiness(entityID, adminUserID, r)
		if err != nil {
			t.Error(err)
		} else if business.Description != r.GetDescription() {
			t.Error("Description doesn't match")
		}
		SetupBusiness(business)
	}

	bs, err := GetCurrentBusinesses(adminUserID)

	if err != nil {
		t.Error(err)
	} else if len(bs) != len(createIn) {
		t.Errorf("Created %d but got %d", len(createIn), len(bs))
	}
}

func TestGetCurrentBusinessWithoutBusiness(t *testing.T) {
	_, err := GetCurrentBusinesses(uuid.NewV4())

	if err == nil {
		t.Error("No error returned")
	}
}

func TestGetBusinesses(t *testing.T) {
	entityID := uuid.NewV4()
	adminUserID := uuid.NewV4()
	var ids []uuid.UUID
	createIn := GetNewBusinessesTestData(entityID, adminUserID)
	for _, r := range createIn {
		business, err := CreateBusiness(entityID, adminUserID, r)
		if err != nil {
			t.Error(err)
		} else if business.Description != r.GetDescription() {
			t.Error("Description doesn't match")
		}
		SetupBusiness(business)
		ids = append(ids, business.ID)
	}

	bs, err := GetBusinesses(ids)

	if err != nil {
		t.Error(err)
	} else if len(bs) != len(createIn) {
		t.Errorf("Created %d but got %d", len(createIn), len(bs))
	}
}

func TestGetBusinessesWithoutBusiness(t *testing.T) {
	var ids []uuid.UUID
	ids = append(ids, uuid.NewV4())

	_, err := GetBusinesses(ids)

	if err == nil {
		t.Error("No error returned")
	}
}

func TestGetBusinessesByEntityID(t *testing.T) {
	entityID := uuid.NewV4()
	adminUserID := uuid.NewV4()
	createIn := GetNewBusinessesTestData(entityID, adminUserID)
	for _, r := range createIn {
		business, err := CreateBusiness(entityID, adminUserID, r)
		if err != nil {
			t.Error(err)
		} else if business.Description != r.GetDescription() {
			t.Error("Description doesn't match")
		}
		SetupBusiness(business)
	}

	bs, err := GetBusinessesByEntityID(entityID)

	if err != nil {
		t.Error(err)
	} else if len(bs) != len(createIn) {
		t.Errorf("Created %d but got %d", len(createIn), len(bs))
	}
}

func TestGetBusinessesByEntityIDWithoutBusiness(t *testing.T) {
	_, err := GetBusinessesByEntityID(uuid.NewV4())

	if err == nil {
		t.Error("No error returned")
	}
}

func TestUpdateBusiness(t *testing.T) {
	entityID := uuid.NewV4()
	adminUserID := uuid.NewV4()
	createIn := GetNewBusinessesTestData(entityID, adminUserID)
	business, err := CreateBusiness(entityID, adminUserID, createIn[0])
	if err != nil {
		t.Error(err)
	} else if business.Description != createIn[0].GetDescription() {
		t.Error("Description doesn't match")
	}
	SetupBusiness(business)
	updateIn := GetUpdateBusinessTestData(adminUserID)

	ud, err := UpdateBusiness(business.ID, updateIn)

	if err != nil {
		t.Error(err)
	} else if ud.DeliveryFee == business.DeliveryFee {
		t.Error("Failed to update")
	}
}

func TestUpdateWithoutBusiness(t *testing.T) {
	adminUserID := uuid.NewV4()
	updateIn := GetUpdateBusinessTestData(adminUserID)

	_, err := UpdateBusiness(uuid.NewV4(), updateIn)

	if err == nil {
		t.Error("No error returned")
	}
}

func TestDeleteBusiness(t *testing.T) {
	entityID := uuid.NewV4()
	adminUserID := uuid.NewV4()
	createIn := GetNewBusinessesTestData(entityID, adminUserID)
	business, err := CreateBusiness(entityID, adminUserID, createIn[0])
	if err != nil {
		t.Error(err)
	} else if business.Description != createIn[0].GetDescription() {
		t.Error("Description doesn't match")
	}
	SetupBusiness(business)

	result, err := DeleteBusiness(business.ID)

	if err != nil {
		t.Error(err)
	} else if !result {
		t.Error("Result is false")
	}
}

func TestDeleteBusinessWithoutBusiness(t *testing.T) {
	result, err := DeleteBusiness(uuid.NewV4())

	if err == nil {
		t.Error("No error returned")
	} else if result {
		t.Error("Result is true")
	}
}
