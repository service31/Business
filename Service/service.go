package service

import (
	"context"
	"fmt"
	"strings"

	"github.com/go-redis/redis"
	"github.com/gogo/protobuf/types"
	"github.com/gogo/status"
	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	uuid "github.com/satori/go.uuid"
	common "gitlab.com/service31/Common"
	logger "gitlab.com/service31/Common/Logger"
	redisClient "gitlab.com/service31/Common/Redis"
	entity "gitlab.com/service31/Data/Entity/Business"
	module "gitlab.com/service31/Data/Module/Business"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

//CreateBusiness create category
func CreateBusiness(entityID, adminUserID uuid.UUID, in *module.CreateBusinessRequest) (*entity.Business, error) {
	business := &entity.Business{
		EntityID:           entityID,
		AdminUserID:        adminUserID,
		Name:               strings.Trim(in.GetName(), " "),
		Description:        strings.Trim(in.GetDescription(), " "),
		BannerImageURL:     strings.Trim(in.GetBannerImageURL(), " "),
		LogoURL:            strings.Trim(in.GetLogoURL(), " "),
		DeliveryFee:        in.GetDeliveryFee(),
		IsActive:           in.GetIsActive(),
		IsFixedDeliveryFee: in.GetIsFixedDeliveryFee(),
	}

	for _, o := range in.GetOpenSchedule() {
		start, err := types.TimestampFromProto(o.Start)
		if logger.CheckError(err) {
			return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Failed to get datetime from %v", o.Start.GoString()))
		}
		end, err := types.TimestampFromProto(o.End)
		if logger.CheckError(err) {
			return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Failed to get datetime from %v", o.End.GoString()))
		}
		temp := &entity.OpenSchedule{
			Day:      module.ScheduleDay_value[o.GetDay().String()],
			IsActive: true,
			IsAllDay: o.IsAllDay,
			Start:    start,
			End:      end,
		}
		business.OpenSchedule = append(business.OpenSchedule, temp)
	}

	for _, o := range in.GetCloseSchedule() {
		date, err := types.TimestampFromProto(o.Date)
		if logger.CheckError(err) {
			return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Failed to get datetime from %v", o.Date.GoString()))
		}
		start, err := types.TimestampFromProto(o.Start)
		if logger.CheckError(err) {
			return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Failed to get datetime from %v", o.Start.GoString()))
		}
		end, err := types.TimestampFromProto(o.End)
		if logger.CheckError(err) {
			return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Failed to get datetime from %v", o.End.GoString()))
		}
		temp := &entity.CloseSchedule{
			Date:     date,
			IsActive: true,
			IsAllDay: o.IsAllDay,
			Start:    start,
			End:      end,
		}
		business.CloseSchedule = append(business.CloseSchedule, temp)
	}

	if db := common.DB.Set("gorm:auto_preload", true).Save(business).Scan(&business); logger.CheckError(db.Error) {
		return nil, status.Error(codes.Internal, "Failed to create business")
	}

	logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.BusinessChange, business.ID.String()))
	return business, nil
}

//GetDetailedBusiness get business by ID
func GetDetailedBusiness(id uuid.UUID) (*entity.Business, error) {
	return getBusinessByID(id)
}

//GetCurrentBusinesses get businesses by claim
func GetCurrentBusinesses(userID uuid.UUID) ([]*entity.Business, error) {
	return getBusinessesByEntityID(userID)
}

//GetBusinesses get business by id []
func GetBusinesses(id []uuid.UUID) ([]*entity.Business, error) {
	results, err := getBusinessesByIDs(id)
	if err != nil {
		return nil, err
	} else if len(results) < 1 {
		return nil, status.Error(codes.NotFound, "")
	}
	return results, nil
}

//GetBusinessesByEntityID get businesses by entity ID
func GetBusinessesByEntityID(entityID uuid.UUID) ([]*entity.Business, error) {
	return getBusinessesByEntityID(entityID)
}

//UpdateBusiness update business
func UpdateBusiness(id uuid.UUID, in *module.UpdateBusinessRequest) (*entity.Business, error) {
	business, err := getBusinessByID(id)
	if err != nil {
		return nil, err
	}
	adminUserID, err := uuid.FromString(in.GetAdminUserID())
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, "Bad AdminUserID")
	}
	business.AdminUserID = adminUserID
	business.Name = strings.Trim(in.GetName(), " ")
	business.Description = strings.Trim(in.GetDescription(), " ")
	business.BannerImageURL = strings.Trim(in.GetBannerImageURL(), " ")
	business.LogoURL = strings.Trim(in.GetLogoURL(), " ")
	business.DeliveryFee = in.GetDeliveryFee()
	business.IsActive = in.GetIsActive()
	business.IsFixedDeliveryFee = in.GetIsFixedDeliveryFee()

	for _, o := range in.GetOpenSchedule() {
		start, err := types.TimestampFromProto(o.Start)
		if logger.CheckError(err) {
			return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Failed to get datetime from %v", o.Start.GoString()))
		}
		end, err := types.TimestampFromProto(o.End)
		if logger.CheckError(err) {
			return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Failed to get datetime from %v", o.End.GoString()))
		}
		temp := &entity.OpenSchedule{
			Day:      module.ScheduleDay_value[o.GetDay().String()],
			IsActive: true,
			IsAllDay: o.IsAllDay,
			Start:    start,
			End:      end,
		}
		business.OpenSchedule = append(business.OpenSchedule, temp)
	}

	for _, o := range in.GetCloseSchedule() {
		date, err := types.TimestampFromProto(o.Date)
		if logger.CheckError(err) {
			return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Failed to get datetime from %v", o.Date.GoString()))
		}
		start, err := types.TimestampFromProto(o.Start)
		if logger.CheckError(err) {
			return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Failed to get datetime from %v", o.Start.GoString()))
		}
		end, err := types.TimestampFromProto(o.End)
		if logger.CheckError(err) {
			return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Failed to get datetime from %v", o.End.GoString()))
		}
		temp := &entity.CloseSchedule{
			Date:     date,
			IsActive: true,
			IsAllDay: o.IsAllDay,
			Start:    start,
			End:      end,
		}
		business.CloseSchedule = append(business.CloseSchedule, temp)
	}

	if db := common.DB.Set("gorm:auto_preload", true).Save(business).Scan(&business); logger.CheckError(db.Error) {
		return nil, status.Error(codes.Internal, "Failed to update business")
	}

	logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.BusinessChange, business.ID.String()))
	return business, nil
}

//DeleteBusiness delete business by ID
func DeleteBusiness(id uuid.UUID) (bool, error) {
	business, err := getBusinessByID(id)
	if err != nil {
		return false, err
	}
	business.IsActive = false
	if db := common.DB.Set("gorm:auto_preload", true).Save(business); logger.CheckError(db.Error) {
		return false, status.Error(codes.Internal, "Failed to delete business")
	}

	logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.BusinessChange, business.ID.String()))
	return true, nil
}

//StreamServerAuthInterceptor address stream interceptor
func StreamServerAuthInterceptor() grpc.StreamServerInterceptor {
	return func(srv interface{}, stream grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler) error {
		newCtx, err := common.Authenticate(stream.Context())
		if err != nil {
			return err
		}
		wrapped := grpc_middleware.WrapServerStream(stream)
		wrapped.WrappedContext = newCtx
		return handler(srv, wrapped)
	}
}

//UnaryServerAuthInterceptor user unary interceptor
func UnaryServerAuthInterceptor() grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		newCtx, err := common.Authenticate(ctx)
		if err != nil {
			return nil, err
		}
		return handler(newCtx, req)
	}
}
func getBusinessByID(id uuid.UUID) (*entity.Business, error) {
	Business := &entity.Business{}
	key := redisClient.RedisMapKeys.GetBusinessKey(id.String())
	err := redisClient.GetRedisValue(key, Business)
	if err == redis.Nil {
		return nil, status.Error(codes.NotFound, "")
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Failed to get Business ID: %s", id.String()))
	}
	return Business, nil
}

func getBusinessesByIDs(ids []uuid.UUID) ([]*entity.Business, error) {
	businesses := []*entity.Business{}
	var keys []string
	for _, id := range ids {
		keys = append(keys, redisClient.RedisMapKeys.GetBusinessKey(id.String()))
	}
	vals, err := redisClient.GetRedisValues(keys)
	if err == redis.Nil {
		return businesses, nil
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, "Failed to get Businesses")
	}
	for _, v := range vals {
		if v != nil {
			business := &entity.Business{}
			err = business.UnmarshalBinary([]byte(fmt.Sprintf("%v", v)))
			if !logger.CheckError(err) {
				businesses = append(businesses, business)
			}
		}
	}
	return businesses, nil
}

func getBusinessesByEntityID(entityID uuid.UUID) ([]*entity.Business, error) {
	businesses := []*entity.Business{}
	mapVals, err := redisClient.GetRedisHashScanValues(redisClient.RedisMapKeys.BusinessHashMap,
		redisClient.RedisMapKeys.GetBusinessesByEntityMapKey(entityID.String()))
	if err == redis.Nil {
		return businesses, nil
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Failed to get Businesses for ID %s", entityID.String()))
	}
	vals, err := redisClient.GetRedisValues(mapVals)
	if err == redis.Nil {
		return businesses, nil
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Failed to get Businesses for ID %s", entityID.String()))
	}
	for _, v := range vals {
		if v != nil {
			business := &entity.Business{}
			err = business.UnmarshalBinary([]byte(fmt.Sprintf("%v", v)))
			if !logger.CheckError(err) {
				businesses = append(businesses, business)
			}
		}
	}
	return businesses, nil
}
