package service

import (
	uuid "github.com/satori/go.uuid"
	common "gitlab.com/service31/Common"
	logger "gitlab.com/service31/Common/Logger"
	redisClient "gitlab.com/service31/Common/Redis"
	entity "gitlab.com/service31/Data/Entity/Business"
	module "gitlab.com/service31/Data/Module/Business"
)

//SetupTesting Init test
func SetupTesting() {
	common.BootstrapTesting(false)
}

//DoneTesting close all connections
func DoneTesting() {
	defer common.DB.Close()
	defer redisClient.RedisClient.Close()
}

//GetNewBusinessesTestData test data
func GetNewBusinessesTestData(entityID, adminUserID uuid.UUID) []*module.CreateBusinessRequest {
	var requests []*module.CreateBusinessRequest
	for i := 0; i < 5; i++ {
		requests = append(requests, &module.CreateBusinessRequest{
			EntityID:           entityID.String(),
			AdminUserID:        adminUserID.String(),
			Name:               common.GetRandomString(10),
			Description:        common.GetRandomString(100),
			BannerImageURL:     "https://img.zh-code.com/img.png",
			LogoURL:            "https://img.zh-code.com/img.png",
			DeliveryFee:        7.99,
			IsActive:           true,
			IsFixedDeliveryFee: true,
		})
	}
	return requests
}

//GetUpdateBusinessTestData test data
func GetUpdateBusinessTestData(adminUserID uuid.UUID) *module.UpdateBusinessRequest {
	return &module.UpdateBusinessRequest{
		AdminUserID:        adminUserID.String(),
		Name:               common.GetRandomString(10),
		Description:        common.GetRandomString(100),
		BannerImageURL:     "https://img.zh-code.com/img.png",
		LogoURL:            "https://img.zh-code.com/img.png",
		DeliveryFee:        10.99,
		IsActive:           true,
		IsFixedDeliveryFee: true,
	}
}

//SetupBusiness setup business
func SetupBusiness(business *entity.Business) {
	key := redisClient.RedisMapKeys.GetBusinessKey(business.ID.String())
	businessMapKey := redisClient.RedisMapKeys.GetBusinessMapKey(business.EntityID.String(), business.ID.String())
	businessUserMapKey := redisClient.RedisMapKeys.GetBusinessMapKey(business.AdminUserID.String(), business.ID.String())
	bytes, err := business.MarshalBinary()
	logger.CheckFatal(err)
	redisClient.SetRedisValue(key, bytes)
	redisClient.SetRedisHashMapValue(redisClient.RedisMapKeys.BusinessHashMap, businessMapKey, key)
	redisClient.SetRedisHashMapValue(redisClient.RedisMapKeys.BusinessHashMap, businessUserMapKey, key)
}
